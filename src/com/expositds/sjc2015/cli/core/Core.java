package com.expositds.sjc2015.cli.core;

import java.io.IOException;

import com.expositds.sjc2015.cli.controllers.ConsoleController;
import com.expositds.sjc2015.cli.model.CentralLibraryIndex;
import com.expositds.sjc2015.cli.views.ConsoleView;

public class Core {
	
	public static void main(String[] args) throws IOException {

		//init all
		CentralLibraryIndex cli = CentralLibraryIndex.getInstance();
		ConsoleController consoleController = ConsoleController.getInstance();
		ConsoleView consoleView = ConsoleView.getInstance();
		
		while (true) {
			consoleController.workOnce(cli);
			consoleView.printAnswersToConsole(cli);
		}
		
		
	}
}
