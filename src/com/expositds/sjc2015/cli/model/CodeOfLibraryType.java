package com.expositds.sjc2015.cli.model;

public enum CodeOfLibraryType {
	CSV,
	Text,
	DB
}
