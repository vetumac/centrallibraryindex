package com.expositds.sjc2015.cli.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;


public class CSVLibrary extends AbstractFileLibrary implements Library{
	
	private String libraryName;
	private final String DIRECTORY_PREFIX = CodeOfLibraryType.CSV + "_";
	 
	public CSVLibrary(String libraryName) throws IOException {
		this.libraryName = libraryName;
		
		//read library files
		File libraryDirectory = new File(CentralLibraryIndex.CLI_DATA_PATH + DIRECTORY_PREFIX + libraryName);
		File[] filesArray = libraryDirectory.listFiles();

		//read info from every file
		if (filesArray != null)
			for (File file : filesArray) {
				addBookIndexFromCSVFile(file);
			}
	}

	private void addBookIndexFromCSVFile(File file) throws IOException {

		//parameters for new book
		int id = -1;
		String author = null;
		String bookname = null;
		LocalDate issued = null;
		String abonent = null;

		try (BufferedReader fromFile = new BufferedReader(new FileReader(file))) {
			//read first line
			String record = fromFile.readLine();

			//read each line
			while (record != null) {
				String[] bookCells = record.split(",");

				id = Integer.valueOf(bookCells[0]);
				if (id < 0) throw new IllegalArgumentException("Illegal file format '" + file + "':Book ID must be > 0");

				author = bookCells[1];
				if (author == null) throw new IllegalArgumentException("Illegal file format '" + file + "': Author must be not null");

				bookname = bookCells[2];
				if (bookname == null) throw new IllegalArgumentException("Illegal file format '" + file + "': Bookname must be not null");

				if (bookCells.length > 3) issued = getLocalDateFromString(bookCells[3]);

				if (bookCells.length > 4) abonent = bookCells[4];
				
				listOfBooks.add(new Book(id, author, bookname, issued, abonent, file));

				//parameters for new book
				id = -1;
				author = null;
				bookname = null;
				issued = null;
				abonent = null;
				
				record = fromFile.readLine();
			}

			

		}
	}

	

	@Override
	protected void saveBook(Book savedBook) throws IOException {
		
		//search file for change
		File currentFile = savedBook.getFile();
		
		//prepare list for record to file
		List<Book> currentFileList = new LinkedList<>();
		for (Book currentBook : listOfBooks) {
			if (currentBook.getFile().equals(currentFile)) currentFileList.add(currentBook);
		};
		
		//write list of books to file
		try (BufferedWriter toFile = new BufferedWriter(new FileWriter(currentFile))) {
			for (Book currentBook : currentFileList) {
				String record;
				if (currentBook.getIssued() != null) record = currentBook.getId() + ","
						+ currentBook.getAuthor() + ","
						+ currentBook.getBookname() + ","
						+ currentBook.getIssued().format(dateFormatter) + ","
						+ currentBook.getAbonent() + "\n";
				else record = currentBook.getId() + ","
						+ currentBook.getAuthor() + ","
						+ currentBook.getBookname() + ",,\n";
				toFile.write(record);
			}
		}
		
	}
	
	@Override
	public String getName() {
		return libraryName;
	}

}