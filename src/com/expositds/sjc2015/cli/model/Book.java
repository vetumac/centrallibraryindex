package com.expositds.sjc2015.cli.model;

import java.io.File;
import java.time.LocalDate;

public class Book {
	private int id;
	private String author;
	private String bookname;
	private LocalDate issued;
	private String abonent;
	private File file;
	
	public Book(int index, String author, String bookname) {
		this.id = index;
		this.author = author;
		this.bookname = bookname;
		this.issued = null;
		this.abonent = null;
	}
	
	public Book(int index, String author, String bookname, LocalDate issued, String abonent, File file) {
		this.id = index;
		this.author = author;
		this.bookname = bookname;
		this.issued = issued;
		this.abonent = abonent;
		this.file = file;
	}

	public LocalDate getIssued() {
		return issued;
	}

	public void setIssued(LocalDate issued) {
		this.issued = issued;
	}

	public String getAbonent() {
		return abonent;
	}

	public void setAbonent(String abonent) {
		this.abonent = abonent;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public int getId() {
		return id;
	}

	public String getAuthor() {
		return author;
	}

	public String getBookname() {
		return bookname;
	}

	
	
	
	
}
