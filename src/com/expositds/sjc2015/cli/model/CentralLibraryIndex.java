package com.expositds.sjc2015.cli.model;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import com.expositds.sjc2015.cli.views.Answer;
import com.expositds.sjc2015.cli.views.CodeOfAnswer;

public class CentralLibraryIndex {
	public static final String CLI_DATA_PATH = "cli\\";
	private static CentralLibraryIndex cli;
	private List<Library> librarysList = new LinkedList<>();
	public List<Answer> answersList = new LinkedList<>();
	
	//get list of directories and build library
	private CentralLibraryIndex() throws IOException {
		File currentFiles[] = new File(CLI_DATA_PATH).listFiles();
		LibraryCreator libraryCreator = new LibraryCreator();
		for (File currentFile : currentFiles) {
			String[] directoryNameParts = currentFile.toString().replace(CLI_DATA_PATH, "").split("_");
			librarysList.add(libraryCreator.getLibrary(CodeOfLibraryType.valueOf(directoryNameParts[0]), directoryNameParts[1]));
		}
		
	}
	
	public static CentralLibraryIndex getInstance( ) throws IOException {
	    if (cli == null) return cli = new CentralLibraryIndex();  
		return cli;
	}
	
	public void find(String author, String bookname) throws IOException {

		List<Answer> listOfAnswersOfBookFound = new LinkedList<>();

		//search books
		for (Library currentLibrary : librarysList) {
			listOfAnswersOfBookFound.addAll(addLibToAnswer(currentLibrary.find(author, bookname), currentLibrary));
		}
		
		LinkedList<Answer> freeBooks = new LinkedList<>();
		LinkedList<Answer> issuedBooks = new LinkedList<>();
		
		for (Answer cuurentAnswer : listOfAnswersOfBookFound) {
			if (cuurentAnswer.getCodeOfAnswer() == CodeOfAnswer.FOUNDMISSING) issuedBooks.add(cuurentAnswer);
			else if (cuurentAnswer.getCodeOfAnswer() == CodeOfAnswer.FOUND) freeBooks.add(cuurentAnswer);
		}
		
		if (!freeBooks.isEmpty()) answersList = freeBooks;
		else if (!issuedBooks.isEmpty()) answersList = issuedBooks;
		else answersList.add(new Answer(CodeOfAnswer.NOTFOUND));
		
	}
	
	public void order(int id, String abonent) throws IOException {
		for (Library currentLibrary : librarysList) {
			Answer currentAnswer = currentLibrary.order(id, abonent);
			if (currentAnswer.getCodeOfAnswer() != CodeOfAnswer.NOTFOUND) {
				answersList.add(currentAnswer);
				return;
			}
		}
		
		answersList.add(new Answer(CodeOfAnswer.NOTFOUND));
	}
	
	public void returnBook(int id) throws IOException {
		for (Library currentLibrary : librarysList) {
			Answer currentAnswer = currentLibrary.returnBook(id);
			if (currentAnswer.getCodeOfAnswer() != CodeOfAnswer.NOTFOUND) {
				answersList.add(currentAnswer);
				return;
			}
		}
		
		answersList.add(new Answer(CodeOfAnswer.NOTFOUND));
	}

	private List<Answer> addLibToAnswer(List<Answer> answers, Library lib) {
		for (Answer currentAnswer : answers) {
			String desc = currentAnswer.getDesc();
			String newDesc = desc.replace("lib=", "lib=" + lib.getName());
			currentAnswer.setDesc(newDesc);
		}
		return answers;
	}

		
	

}
