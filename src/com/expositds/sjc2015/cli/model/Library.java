package com.expositds.sjc2015.cli.model;

import java.io.IOException;
import java.util.List;

import com.expositds.sjc2015.cli.views.Answer;

public interface Library {
	
	public List<Answer> find(String author, String bookname) throws IOException;
	public Answer order(int id, String abonent) throws IOException;
	public Answer returnBook(int id) throws IOException;
	public String getName();
	

}
