package com.expositds.sjc2015.cli.model;

import java.io.IOException;

public class LibraryCreator {

	public Library getLibrary(CodeOfLibraryType codeOfLibraryType,	String libraryName, String connectionString) throws IOException {
		switch (codeOfLibraryType) {
			case CSV:
				return new CSVLibrary(libraryName);
			case Text:
				return new TextLibrary(libraryName);
			case DB:
				throw new IllegalArgumentException("DB library not yet implemented");
		}
		return null;
		
	}
	
	public Library getLibrary(CodeOfLibraryType codeOfLibraryType,	String libraryName) throws IOException {
		return getLibrary(codeOfLibraryType, libraryName, null);
	}

}
