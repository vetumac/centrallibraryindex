package com.expositds.sjc2015.cli.model;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

import com.expositds.sjc2015.cli.views.Answer;
import com.expositds.sjc2015.cli.views.CodeOfAnswer;


public abstract class AbstractFileLibrary implements Library {
	
	protected List<Book> listOfBooks = new LinkedList<>();
	protected DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");

	@Override
	public List<Answer> find(String author, String bookname) throws IOException {
		
		List<Answer> listAnswersOfBooksFound = new LinkedList<>();
		
		//search books
		for (Book currentBook : listOfBooks) {
			if (author != null && (currentBook.getAuthor().contains(author)) 
					|| (bookname != null && currentBook.getBookname().contains(bookname))) {
				if (currentBook.getIssued() != null) listAnswersOfBooksFound.add(new Answer(
						CodeOfAnswer.FOUNDMISSING, 
						"id=" + currentBook.getId() + 
						" lib=" +
						" issued=" + currentBook.getIssued().format(dateFormatter)));
				else listAnswersOfBooksFound.add(new Answer(
						CodeOfAnswer.FOUND, 
						"id=" + currentBook.getId() + " lib="));
			}
		}
		
		return listAnswersOfBooksFound;
	}

	@Override
	public Answer order(int id, String abonent) throws IOException {
		
		Book orderedBook = null;
		
		//search book
		for (Book currentBook : listOfBooks) {
			if (currentBook.getId() == id) {
				orderedBook = currentBook;
				break;
			}
		};
		
		if (orderedBook == null) return new Answer(CodeOfAnswer.NOTFOUND);
		if (orderedBook.getAbonent() != null) return new Answer(CodeOfAnswer.RESERVED,
				"abonent=" + orderedBook.getAbonent() + " date=" + orderedBook.getIssued().format(dateFormatter));
		
		orderedBook.setAbonent(abonent);
		orderedBook.setIssued(LocalDate.now());
		
		saveBook(orderedBook);
		return new Answer(CodeOfAnswer.OK, "abonent=" + abonent + " date=" + LocalDate.now().format(dateFormatter));
	}

	@Override
	public Answer returnBook(int id) throws IOException {
		Book returnedBook = null;

		//search book
		for (Book currentBook : listOfBooks) {
			if (currentBook.getId() == id) {
				returnedBook = currentBook;
				break;
			}
		};
		
		if (returnedBook == null) return new Answer(CodeOfAnswer.NOTFOUND);
		if (returnedBook.getAbonent() == null) return new Answer(CodeOfAnswer.ALREADYRETURNED);
		
		String abonent = returnedBook.getAbonent();
		returnedBook.setAbonent(null);
		returnedBook.setIssued(null);
		
		saveBook(returnedBook);
		
		return new Answer(CodeOfAnswer.OK, "abonent=" + abonent);
	}
	
	protected LocalDate getLocalDateFromString(String date) {
		if (date != null) return LocalDate.parse(date, dateFormatter);
		else return null;
	}
	
	protected abstract void saveBook(Book savedBook) throws IOException;

}
