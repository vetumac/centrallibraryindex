package com.expositds.sjc2015.cli.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;

public class TextLibrary extends AbstractFileLibrary implements Library {
	
	//names of fields in text file
	private final String ID_FIELD_NAME = "Index";
	private final String AUTHOR_FIELD_NAME = "Author";
	private final String BOOKNAME_FIELD_NAME = "Name";
	private final String ISSUED_FIELD_NAME = "Issued";
	private final String ABONENT_FIELD_NAME = "Issuedto";

	private String libraryName;
	private final String DIRECTORY_PREFIX = CodeOfLibraryType.Text + "_";

	public TextLibrary(String libraryName) throws IOException {
		
		this.libraryName = libraryName;
		
		//read library files
		File libraryDirectory = new File(CentralLibraryIndex.CLI_DATA_PATH + DIRECTORY_PREFIX + libraryName);
		File[] filesArray = libraryDirectory.listFiles();
		
		//read info from every file
		if (filesArray != null)
			for (File file : filesArray) {
				addBookIndexFromTextFile(file);
			}
		
	}

	private void addBookIndexFromTextFile(File file) throws IOException {
		
		//parameters for new book
		int id = -1;
		String author = null;
		String bookname = null;
		LocalDate issued = null;
		String abonent = null;
		
		
		try (BufferedReader fromFile = new BufferedReader(new FileReader(file))) {
			//read first line
			String record = fromFile.readLine();

			//read each line
			while (record != null) {
				String[] parameter = record.split("=");

				if (parameter[0].equals(ID_FIELD_NAME)) {
					id = Integer.valueOf(parameter[1]);
					if (id < 0) throw new IllegalArgumentException("Illegal file format '" + file + "':Book ID must be > 0");
				} else if (parameter[0].equals(AUTHOR_FIELD_NAME)) {
					author = parameter[1];
					if (author == null) throw new IllegalArgumentException("Illegal file format '" + file + "': Author must be not null");
				} else if (parameter[0].equals(BOOKNAME_FIELD_NAME)) {
					bookname = parameter[1];
					if (bookname == null) throw new IllegalArgumentException("Illegal file format '" + file + "': Bookname must be not null");
				} else if (parameter.length > 1 && parameter[0].equals(ISSUED_FIELD_NAME)) {
					issued = getLocalDateFromString(parameter[1]);
				} else if (parameter.length > 1 && parameter[0].equals(ABONENT_FIELD_NAME)) {
					abonent = parameter[1];
				}
				record = fromFile.readLine();
			};
		}
		
		listOfBooks.add(new Book(id, author, bookname, issued, abonent, file));

	}

	@Override
	protected void saveBook(Book savedBook) throws IOException {
		
		//search file for change
		File currentFile = savedBook.getFile();
		
		try (BufferedWriter toFile = new BufferedWriter(new FileWriter(currentFile))) {
		
				String record = ID_FIELD_NAME + "=" + savedBook.getId() + "\n" 
							  + AUTHOR_FIELD_NAME + "=" + savedBook.getAuthor() + "\n"
							  + BOOKNAME_FIELD_NAME + "=" + savedBook.getBookname() + "\n"
							  + ISSUED_FIELD_NAME + "=" + savedBook.getIssued().format(dateFormatter) + "\n"
							  + ABONENT_FIELD_NAME + "=" + savedBook.getAbonent() + "\n";
				toFile.write(record);
		
		}

		
	}

	@Override
	public String getName() {
		return libraryName;
	}

	
}
