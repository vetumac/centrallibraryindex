package com.expositds.sjc2015.cli.controllers;

public enum CodeOfCommand {
	FIND,
	ORDER,
	RETURN,
	EXIT
}
