package com.expositds.sjc2015.cli.controllers;

import java.util.Map;
import java.util.TreeMap;

public class Command {
	private CodeOfCommand codeOfCommand;
	private Map<CodeOfAtribute, String> atributes = new TreeMap<>();
	
	public Command(String commandStr) {
		String[] commandParts = commandStr.split(" ");
		codeOfCommand = CodeOfCommand.valueOf(commandParts[0]);
		
		int commandPartsCount = 1;
		while (commandParts.length > commandPartsCount) {
			String atribute[] = commandParts[commandPartsCount].split("=");
			if (commandParts.length > 1) atributes.put(CodeOfAtribute.valueOf(atribute[0]), atribute[1]);
			commandPartsCount++;
		}
		
	}
	
	public boolean validateCommand() {
		
		switch (codeOfCommand) {
		case FIND : return (atributes.containsKey(CodeOfAtribute.author) || atributes.containsKey(CodeOfAtribute.name)) 
					&& (!atributes.containsKey(CodeOfAtribute.id) && !atributes.containsKey(CodeOfAtribute.abonent));
		case ORDER : return (atributes.containsKey(CodeOfAtribute.id) && atributes.containsKey(CodeOfAtribute.abonent))
					 && (!atributes.containsKey(CodeOfAtribute.author) && !atributes.containsKey(CodeOfAtribute.name));
		case RETURN : return atributes.containsKey(CodeOfAtribute.id) 
					  && (!atributes.containsKey(CodeOfAtribute.abonent) && !atributes.containsKey(CodeOfAtribute.author) && !atributes.containsKey(CodeOfAtribute.name));
		case EXIT : return atributes.isEmpty();
		}
		
		
		return true;
	}

	public CodeOfCommand getCodeOfCommand() {
		return codeOfCommand;
	}

	public Map<CodeOfAtribute, String> getAtributes() {
		return atributes;
	}
	
	
	
}
