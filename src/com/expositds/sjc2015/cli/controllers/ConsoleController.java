package com.expositds.sjc2015.cli.controllers;

import java.io.IOException;
import java.util.Scanner;

import com.expositds.sjc2015.cli.model.CentralLibraryIndex;
import com.expositds.sjc2015.cli.views.Answer;
import com.expositds.sjc2015.cli.views.CodeOfAnswer;

public class ConsoleController {
	private static ConsoleController consoleController;
	private Scanner in = new Scanner(System.in);
	
	public static ConsoleController getInstance( ) {
	    if (consoleController == null) return consoleController = new ConsoleController();  
		return consoleController;
	}
	
	public void workOnce(CentralLibraryIndex cli) throws IOException {
		String commandStr = in.nextLine();
		
		try {
			Command command = new Command(commandStr);
			if (command.validateCommand()) {
				switch (command.getCodeOfCommand()) {
				case FIND : {
					String author = command.getAtributes().get(CodeOfAtribute.author);
					String name = command.getAtributes().get(CodeOfAtribute.name);
					cli.find(author, name);
					break;
				}
				case ORDER : {
					int id = Integer.valueOf(command.getAtributes().get(CodeOfAtribute.id));
					String abonent = command.getAtributes().get(CodeOfAtribute.abonent);
					cli.order(id, abonent);
					break;
				}
				case RETURN : {
					int id = Integer.valueOf(command.getAtributes().get(CodeOfAtribute.id));
					cli.returnBook(id);
					break;
				}
				case EXIT : System.exit(0);
				
				}
			} else cli.answersList.add(new Answer(CodeOfAnswer.SYNTAXERROR));
		} catch (IllegalArgumentException e) {
			cli.answersList.add(new Answer(CodeOfAnswer.SYNTAXERROR));
		} catch (NullPointerException e) {
			cli.answersList.add(new Answer(CodeOfAnswer.SYNTAXERROR));
		}
		
		
		
		
	}
	
}
