package com.expositds.sjc2015.cli.views;


public class Answer {
	
	private final CodeOfAnswer CODE_OF_ANSWER;
	private String desc;
	
	public Answer(CodeOfAnswer codeOfAnswer, String desc) {
		this.CODE_OF_ANSWER = codeOfAnswer;
		this.desc = desc;
	}
	
	public Answer(CodeOfAnswer codeOfAnswer) {
		this(codeOfAnswer, null);
	}

	public CodeOfAnswer getCodeOfAnswer() {
		return CODE_OF_ANSWER;
	}

	public String getDesc() {
		return desc;
	}
	
	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		if (desc != null) return CODE_OF_ANSWER + " " + desc;
		else return CODE_OF_ANSWER.toString();
	}
	
	
	
	

}
