package com.expositds.sjc2015.cli.views;

import java.io.IOException;

import com.expositds.sjc2015.cli.model.CentralLibraryIndex;

public class ConsoleView {
	
	private static ConsoleView consoleView;
	
	public static ConsoleView getInstance( ) {
	    if (consoleView == null) return consoleView = new ConsoleView();  
		return consoleView;
	}
	
	public void printAnswersToConsole(CentralLibraryIndex cli) throws IOException {
		for (Answer currentAnswer : cli.answersList) {
			System.out.println(currentAnswer);
		}
		
		cli.answersList.clear();
	}
}
