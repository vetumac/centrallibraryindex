package com.expositds.sjc2015.cli.views;

public enum CodeOfAnswer {
	NOTFOUND,
	ALREADYRETURNED,
	OK,
	RESERVED,
	FOUNDMISSING,
	FOUND,
	SYNTAXERROR
}
